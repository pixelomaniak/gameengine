﻿using System;
using System.Threading;
using Pencil.Gaming;
using GameEngine.Rendering;

namespace GameEngine
{
    class Program
    {
        private static int _width = 500;
        private static int _height = 500;
        private static string _title = "Game Engine";
        private static GlfwWindowPtr _window;
        private bool _isRunning = false;
        private Game _game;
        private int _maxFps = 5000; 
          
        private static void Main(string[] args)
        {
            _window = Window.CreateWindow(_width, _height, _title);
            Console.WriteLine(RenderUtils.GetOpenGLVersion());
            RenderUtils.InitGL();
            Program engine = new Program();
            engine.Start();
        }

        public Program()
        {
            this._game = new Game();
        }

        public void Start()
        {
            if (_isRunning)
                return;
            Console.Write("[ENGINE STARTED]\n");
            _isRunning = true;
            Run();
        }

        public void Stop()
        {
            if (!_isRunning)
                return;
            Console.Write("[ENGINE STOPPED]\n");
            _isRunning = false;
        }

        private void Run()
        {
            float frameTime = 1 / (float)_maxFps;
            long currentTime = 0;
            long prevTime = Time.GetTime();
            long passedTime;
            double accumulatedTime = 0;
            double accumulatedFrameTime = 0;
            int frames = 0;

            while (_isRunning)
            {
                bool shouldRender = false;
                currentTime = Time.GetTime();
                passedTime = currentTime - prevTime;
                prevTime = currentTime;
                accumulatedTime += passedTime * Time.ToSecond;
                accumulatedFrameTime += passedTime;
                while(accumulatedTime >= frameTime)
                {
                    shouldRender = true;
                    accumulatedTime -= frameTime;
                    if (Glfw.WindowShouldClose(_window))
                        StopAndCleanUp();
                    // TODO: input
                    _game.Update();
                    Glfw.PollEvents();
                    Time.Delta = frameTime;
                }
                if(accumulatedFrameTime >= Time.Seconds)
                {
                    Console.WriteLine(frames + " FPS\n");
                    accumulatedFrameTime = 0;
                    frames = 0;
                }
                if (shouldRender)
                {
                    frames++;
                    Render();
                }
                else
                {
                    Thread.Sleep(1);
                }
            }
        }

        private void Render()
        {
            RenderUtils.ClearScreen();
            _game.Render();
            Glfw.SwapBuffers(_window);
        }

        private void StopAndCleanUp()
        {
            Stop();
            CleanUp();
        }

        private void CleanUp()
        {
            Glfw.Terminate();
        }
    }
}
