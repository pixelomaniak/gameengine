﻿namespace GameEngine.Rendering
{
    class Utils
    {
        public static float[] CreateFloatBuffer(Vertex[] vertices)
        {
            float[] result = new float[vertices.Length * Vertex.Size];
            for(int i = 0; i < vertices.Length; i++)
            {
                result[i * Vertex.Size] = vertices[i].Position.X;
                result[i * Vertex.Size + 1] = vertices[i].Position.Y;
                result[i * Vertex.Size + 2] = vertices[i].Position.Z;
            }
            return result;
        }
    }
}
