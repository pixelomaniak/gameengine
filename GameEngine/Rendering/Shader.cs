﻿using System;
using System.Collections.Generic;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace GameEngine.Rendering
{
    class Shader
    {
        private uint _program;
        private Dictionary<string, int> _uniformsMap;

        public Shader()
        {
            this._program = GL.CreateProgram();
            this._uniformsMap = new Dictionary<string, int>();
            if(this._program == 0)
            {
                Console.Error.WriteLine("Shader creation failed. Couldn't find valid memory location");
                Environment.Exit(-1);
            }
        }

        public void AddVertexShader(string text)
        {
            AddShader(text, ShaderType.VertexShader);
        }

        public void AddFragmentShader(string text)
        {
            AddShader(text, ShaderType.FragmentShader);
        }

        public void AddGeometryShader(string text)
        {
            AddShader(text, ShaderType.GeometryShader);
        }

        public void AddUniform(string name)
        {
            int location = GL.GetUniformLocation(_program, name);
            if(location == -1)
            {
                Console.Error.WriteLine("Couldn't find the uniform in the current program.");
                Environment.Exit(-1);
            }
            _uniformsMap.Add(name, location);
        }

        public void SetUniformFloat(string name, float value)
        {
            GL.Uniform1(_uniformsMap[name], value);
        }

        public void SetUniformVector(string name, Vector3 value)
        {
            GL.Uniform3(_uniformsMap[name], value.X, value.Y, value.Z);
        }

        public void SetUniformMatrix(string name, Matrix value)
        {
            GL.UniformMatrix4(_uniformsMap[name], false, ref value);
        }

        public void CompileProgram()
        {
            GL.LinkProgram(_program);
            int status = 0;
            GL.GetProgram((int)_program, ProgramParameter.LinkStatus, out status);
            if (status == 0)
            {
                Console.Error.WriteLine("Couldn't link program.");
                Console.Error.WriteLine(GL.GetShaderInfoLog((int)_program));
                Environment.Exit(-1);
            }
        }

        public void Bind()
        {
            GL.UseProgram(_program);
        }

        private void AddShader(string source, ShaderType type)
        {
            uint shader = GL.CreateShader(type);
            if (shader == 0)
            {
                Console.Error.WriteLine("Couldn't add shader.");
                Environment.Exit(-1);
            }
            GL.ShaderSource(shader, source);
            GL.CompileShader(shader);
            int status = 0;
            GL.GetShader(shader, ShaderParameter.CompileStatus, out status);
            if(status == 0)
            {
                Console.Error.WriteLine("Couldn't compile shader.");
                Console.Error.WriteLine(GL.GetShaderInfoLog((int)shader));
                Environment.Exit(-1);
            }
            GL.AttachShader(_program, shader);
        }
    }
}
