﻿using System;
using Pencil.Gaming.Graphics;

namespace GameEngine.Rendering
{
    class RenderUtils
    {
        public static void ClearScreen()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        public static void InitGL()
        {
            GL.ClearColor(0, 0, 1, 1);
            GL.FrontFace(FrontFaceDirection.Cw); // GL_CW
            GL.CullFace(CullFaceMode.Back); // GL_BACK
            GL.Enable(EnableCap.CullFace); // GL_CULL_FACE
            GL.Enable(EnableCap.DepthTest); // GL_DEPTH_TEST
            GL.Enable(EnableCap.FramebufferSrgb); // GL_FRAMEBUFFER_SRGB auto gamma correction
        }

        public static string GetOpenGLVersion()
        {
            return GL.GetString(StringName.Version);
        }
    }
}
