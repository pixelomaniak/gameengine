﻿using System;
using System.IO;

namespace GameEngine.Rendering
{
    class ResourceLoader
    {
        public static string LoadShader(string fileName)
        {
            StreamReader reader = null;
            string shaderSource = null;
            try
            {
                reader = new StreamReader("./resources/shaders/" + fileName);
                string line;
                while((line = reader.ReadLine()) != null)
                    shaderSource += line + "\n";
            }
            catch(Exception e)
            {
                Console.Error.WriteLine(e.StackTrace);
                Environment.Exit(-1);
            }
            return shaderSource;
        }
    }
}
