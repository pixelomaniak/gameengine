﻿using Pencil.Gaming.MathUtils;

namespace GameEngine.Rendering
{
    class Vertex
    {
        public static int Size = 3;
        private Vector3 _position;
        public Vector3 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public Vertex(Vector3 pos)
        {
            this.Position = pos;
        } 
    }
}
