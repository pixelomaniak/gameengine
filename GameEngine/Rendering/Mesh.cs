﻿using Pencil.Gaming.Graphics;
using System;

namespace GameEngine.Rendering
{
    class Mesh
    {
        private int _vbo;
        private int _ipo;
        private int _size;

        public Mesh()
        {
            this._vbo = GL.GenBuffer();
            this._size = 0;
        }

        public void AddVertices(Vertex[] vertices)
        {
            _size = vertices.Length;
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            float[] data = Utils.CreateFloatBuffer(vertices);
            int bytes = data.Length * sizeof(float);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)bytes, data, BufferUsageHint.StaticDraw);
        }

        public void Draw()
        {
            GL.EnableVertexAttribArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Vertex.Size * 4, 0);
            GL.DrawArrays(BeginMode.Triangles, 0, _size);
            GL.DisableVertexAttribArray(0);
        }
    }
}