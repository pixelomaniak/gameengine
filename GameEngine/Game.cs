﻿using Pencil.Gaming.MathUtils;
using GameEngine.Rendering;

namespace GameEngine
{
    class Game
    {
        private Mesh _mesh;
        private Shader _shader;

        public Game()
        {
            this._mesh = new Mesh();
            Vertex[] data = new Vertex[3];
            data[0] = new Vertex(new Vector3(-1, -1, 0));
            data[1] = new Vertex(new Vector3(0, 1, 0));
            data[2] = new Vertex(new Vector3(1, -1, 0));
            this._mesh.AddVertices(data);
            this._shader = new Shader();
            this._shader.AddVertexShader(ResourceLoader.LoadShader("basic.vs"));
            this._shader.AddFragmentShader(ResourceLoader.LoadShader("basic.fs"));
            this._shader.CompileProgram();
            this._shader.AddUniform("uSize");
            this._shader.AddUniform("uColor");
        }

        public void Update()
        {
            _shader.SetUniformFloat("uSize", 0.5f);
            _shader.SetUniformVector("uColor", new Vector3(1, 1, 1));
        }

        public void Render()
        {
            _shader.Bind();
            _mesh.Draw();
        }
    }
}
