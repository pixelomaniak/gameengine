﻿using System;
using Pencil.Gaming;

namespace GameEngine
{
    class Window
    {
        public static GlfwWindowPtr WindowPtr;

        public static GlfwWindowPtr CreateWindow(int width, int height, string windowTitle)
        {
            if (!Glfw.Init())
            {
                Console.WriteLine("Couldn't initialize a GLFW");
                Console.Error.WriteLine("Press any key to exit...");
                Console.ReadKey();
                Environment.Exit(-1);
            }
            WindowPtr = Glfw.CreateWindow(width, height, windowTitle, GlfwMonitorPtr.Null, GlfwWindowPtr.Null);
            Glfw.MakeContextCurrent(WindowPtr);
            return WindowPtr;
        }
    }
}
