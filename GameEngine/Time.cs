﻿using System;

namespace GameEngine
{
    class Time
    {
        public static readonly double Seconds = 10000000;
        public static readonly double ToSecond = 1 / Seconds;
        private static double _delta;
        public static double Delta
        {
            get
            {
                return _delta;
            }

            set
            {
                _delta = value;
            }
        }

        public static long GetTime()
        {
            return System.DateTime.Now.Ticks;
        }
    }
}
